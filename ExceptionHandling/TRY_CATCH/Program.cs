﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRY_CATCH
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter first number");
            int x = int.Parse(Console.ReadLine());
           Console.WriteLine("enter second number");
            int y = int.Parse(Console.ReadLine());
        try
            {
                int div = x / y;
                Console.WriteLine($"{x} is divide by :{y} is {div}");
            }
            catch (IndexOutOfRangeException exe) 
            {
                Console.WriteLine("Index out of Range Exception ");
            }
            catch (DivideByZeroException exe)
            {
                Console.WriteLine("catch block of Division by zero Exception");
            }
            catch (Exception exe)
            {
                Console.WriteLine("sorry Diviion by zero is  not allowed");
            }
           
            finally
            {
                Console.WriteLine("*******Exception Handling***");
            }
        }

    }
}
