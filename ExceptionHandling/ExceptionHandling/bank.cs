﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandling
{
    internal class bank
    {
        public string name;
        public int account_number;
        public int balance = 2000;

        public bank(string name, int account_number)
        {
            this.name = name;
            this.account_number = account_number;
        }
        public void detail()
        {
            Console.WriteLine($"Babk holder name :{this.name}");
            Console.WriteLine($"Account Number :{this.account_number}");
            Console.WriteLine($"Account balance :{this.balance}");

        }
        public void withdraw()
        {
            Console.WriteLine("Enter the money for withdraw");
            int money = int.Parse(Console.ReadLine());
            try
            {

                if (money > 2000 || money<0)
                {
                    throw new UserdefinedException("Your  money is insufficient");
                }


                else
                {
                    int x;
                    x = balance - money;
                    Console.WriteLine($"Now Your Current Balance is{x}");
                }
            }
            catch (UserdefinedException exe)
            {
                Console.WriteLine(exe.Message);
            }
        }
            

        }


    }

