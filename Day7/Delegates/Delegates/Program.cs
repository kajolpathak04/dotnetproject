﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    internal class Program
    {
        static void Main(string[] args)
        {



            //by using anomous function
            Console.WriteLine("*****By Using a Anomous Function****8");
            Func<int, int, int> ds1=(a, b) => (a * b);
                int result1 = ds1(4, 7);
                Console.WriteLine(result1);
            Action<int, int> ds2 = (a,b) => Console.WriteLine($"{a}is multiply by {b}={a * b}");

            ds2(8, 9);

                Predicate<string> ds3 = (message)=>message.Count()>4 ? true:false;
                bool check = ds3("Delegate");
                if (check)
                {
                    Console.WriteLine("more than 4 letter are present");

                }
                else
                {
                    Console.WriteLine("less than 4 letter are present");
                }


            }
            /*public static int multiple1(int a, int b)
            {
                return a * b;
            }
            public static void multiple2(int a, int b)
            {
            Console.WriteLine($"{a}is multiply by {b}={a * b}");
        }
            public static bool countletter(string msg)
            {
                if (msg.Count() > 4)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            */
        }
    }


    

