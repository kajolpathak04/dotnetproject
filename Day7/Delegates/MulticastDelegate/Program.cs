﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MulticastDelegate
{
    internal class Program
    {
       delegate void MailerDelegate();
        static void Main(string[] args)
        {
            Console.WriteLine("Multicast delegate");

            MailerDelegate mailerDelegate = Internalmail;
            mailerDelegate+= Externalmail ;
            mailerDelegate("Mailer Delegate");
        }
            public static void Externalmail(string msg)
            {
            Console.WriteLine("Exterternal mail");
                Console.WriteLine(msg);
            }
        public static void Internalmail(string msg)
        {
            Console.WriteLine("Internal mail");
            Console.WriteLine(msg);
        }
        }
    }

