﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_Indexer
{
    internal class Program

    {
        static void Main(string[] args)
        {
            Customer customer1 = new Customer("customer1","Bangalore",1001);
            Customer customer2 = new Customer("customer2", "Chennai", 1002);
            Customer customer3 = new Customer("customer3", "Pune", 1003);
            Console.WriteLine($"id::{customer1[1]}\tName::{customer1[2]}\tCity::{customer1[3]}");
            //update
            customer1[2] ="Kajol";
            Console.WriteLine($"id::{customer1[1]}\tName::{customer1[2]}\tCity::{customer1[3]}");

        }
    }
}
