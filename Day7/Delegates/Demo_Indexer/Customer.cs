﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_Indexer
{
    internal class Customer
    {
        string name;
        string city;
        int id;
        public Customer(string name, string city, int id)
        {
            this.name = name;
            this.city = city;
            this.id = id;
        } 
        //<Acess modifier><return type> this[parameter
        public object this [int index]
        {
            get
            { if (index == 1) return id;
                else if (index == 2) return name;
            else if(index == 3) return city;    
            return null;    
            }
            set
            {
                if (index == 1) id = (int)value;
                else if (index == 2) name = (string)value;
                else city = (string)value;
            }

        }
    }
}

