﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_Linq
{
    internal class BestSeller
    {
        public string Name { get; set; }
        public int Rank { get; set; }
    }
}
