﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_Linq
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Product[] productArray = new Product[]
            {
                new Product(){ Name="Nokia",Category="mobile",Make="Nokia",Price=2500 },
                new Product(){ Name="Apple",Category="mobile",Make="Apple",Price=50000 },
                new Product(){Name="RealMe",Category="mobile",Make="RealMe",Price=30000},
                new Product(){Name="RealMe",Category="TV",Make="RealMe",Price=900}

            };

            BestSeller[] bestProduct = new BestSeller[]
            {
                new BestSeller(){Name="Apple",Rank=1},
                new BestSeller(){Name="RealMe",Rank=2}
            };

            //to Display all Element
            Console.WriteLine("******To display all Element*****");
            foreach(Product item in productArray)
            {
                Console.WriteLine($"Name:{item.Name}\tCategory{item.Category}\tPrice:{item.Price}\tMake{item.Make}");

            }
            Product[] productTemp = new Product[5];

            //To Display a name having price >2000 and <20000
            Console.WriteLine("****By normally if-else condition****");
            foreach (Product item in productArray)
            {
                if (item.Price >= 2000 && item.Price <= 20000)
                {

                    Console.WriteLine($"Name:{item.Name}\tCategory::{item.Category}\tPrice:{item.Price}\tMake{item.Make}");

                }
            }
            //print items with the specified price range




            //using Linq method

            //}

            Console.WriteLine("****using a Linux command****");

            Product[] products = productArray.Where(p => p.Price >= 2000 && p.Price <= 20000).ToArray();
            foreach (Product item in products)
            {
                Console.WriteLine($"Name:{item.Name}\tCategory::{item.Category}\tPrice:{item.Price}\tMake::{item.Make}");
            }
            //using query syntax
            Console.WriteLine("By using a Query syntax");
            var filterProduct = (from p in productArray
                                 where p.Price > 2000 && p.Price < 20000
                                 select p).ToArray();
            foreach(Product item in filterProduct)
            {
                Console.WriteLine(item);
            }


            //GetProduct by Category


            Console.WriteLine("*****only mobile Category*****");
            Product[] getProductByCategory = productArray.Where(p => p.Category == "Mobile").ToArray();
            foreach (Product item in getProductByCategory)
            {
                Console.WriteLine($"Name:{item.Name}\tCategory::{item.Category}\tPrice::{item.Price}\tMake::{item.Make}");
            }
            Console.WriteLine("******to get a mobile category****");
            /*var filterProduct1 = (from p in productArray
                                 where p.Category
                                 select p).ToArray();*/
            //----Get First Matching Value From the Array

            Console.WriteLine("Get First Matching value");

          Product[] firstOccurance = productArray.FirstOrDefault(p => p.Name == "RealMe").oString

            foreach (Product item in firstOccurance)
            {
                Console.WriteLine($"Name:{item.Name}\tCategory::{item.Category}\tPrice::{item.Price}\tMake::{item.Make}");
            }

            //Order By
            Console.WriteLine( "Order By Logic");
            var OrderLogic=from p in productArray
            orderby p.Name descending
            select p;

            foreach(Product item in OrderLogic)
            {
                Console.WriteLine($"Name:{item.Name}\tCategory::{item.Category}\tPrice::{item.Price}\tMake::{item.Make}");

            }


            //Grouping operation
            Console.WriteLine("**********Group by**********");
           var groupByCategory= from p in productArray
            group p by p.Category;
            foreach(var item in groupByCategory)
            {
                Console.WriteLine($"group by Category::{item.Key}");
                foreach(var item1 in item)
                {
                    Console.WriteLine($"Name:{item1.Name}\tCategory::{item1.Category}\tPrice::{item1.Price}\tMake::{item1.Make}");
                }




                //Join operation

                Console.WriteLine("******Join Operation***");
               var result= from b in bestProduct
                join p in productArray
                on b.Name equals p.Name
                select new
                {
                    productName = p.Name,
                    bestProductname = b.Rank
                };
                foreach (var item2 in result)
                {
                    Console.WriteLine(item2.productName+"\t "+item2.bestProductname);
                }


                //Max and minimum value
                Console.WriteLine("Maximum value");
                var highestPrice = productArray.Max(p => p.Price);
                Console.WriteLine(highestPrice);


                //count element
                Console.WriteLine("*******count rows******");
                var countItems = productArray.Count();
                Console.WriteLine(countItems);



                //return from list
                Console.WriteLine("****Return from list****");
                List<int> myValues = new List<int> { 101, 102, 103, 104, 105 };
                int getEvenCount = myValues.Count(e => e % 2 == 0);
                Console.WriteLine(getEvenCount);


                //contains 
                Console.WriteLine("***TO check a contain");
                string[] cities = { "bagalore", "bhopal", "Mysore", "Delhi", "bombay" };


                var result2 = from city in cities
                where city.Contains('b')
                select city;
                foreach(var c in result2)
                {
                    Console.WriteLine(c);
                }



                //take function



            }
        }
    }

}