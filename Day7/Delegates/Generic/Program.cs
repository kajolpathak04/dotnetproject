﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic
{
  
    internal class Program
    {
        static void Main() { 
            Func<int,int,int> ds1 = multiple1;
            int result1 = ds1(4, 7);
            Console.WriteLine(result1);
            Action<int,int> ds2 = multiple2;
             ds2(8, 9);

            Predicate<string> ds3=countletter;
           bool check= ds3("Delegate");
            if (check)
            {
                Console.WriteLine("more than 4 letter are present");

            }
            else
            {
                Console.WriteLine("less than 4 letter are present");
            }


        }
        public static  int multiple1(int a,int b)
        {
            return a * b;
        }
        public static  void multiple2(int a, int b)
        {
            Console.WriteLine($"{a}is multiply by {b}={a*b}"  );
        }
        public static bool countletter(string msg)
        {
            if (msg.Count() > 4)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
