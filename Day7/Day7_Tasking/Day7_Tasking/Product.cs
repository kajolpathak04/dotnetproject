﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7_Tasking
{
    internal class Product
    {
        public int id { get; set; }
        public string name { get; set; }
        public int price { get; set; }  
    }
}
