﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7_Tasking
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ShoppingList[] shoppingLists = new ShoppingList[]
            {
                new ShoppingList(){id=101,quantity=3},
                new ShoppingList(){id=102,quantity=8},
                new ShoppingList(){id=103,quantity=9},
                new ShoppingList(){id=105,quantity=4}
            };
            Product[] products = new Product[]
            {
                new Product(){id=102,name="TV",price=560},
                 new Product(){id=103,name="TV",price=960},
                  new Product(){id=104,name="Laptop",price=60}
            };

            Console.WriteLine("***All Shopping List Data***** ");
            foreach (ShoppingList item in shoppingLists)
            {
                Console.WriteLine($"id::{item.id}\tquantity::{item.quantity}");
            }



            Console.WriteLine("***All Product List Data***** ");
            foreach (Product item1 in products)
            {
                Console.WriteLine($"id::{item1.id}\tName::{item1.name}\tprice::{item1.price}");
            }

            Console.WriteLine("******Join Operation***");
            var result = from p in products
                         join s in shoppingLists
                         on p.id equals s.id
                         select new 
                         {
                             productID = p.id,
                             productName = p.name,
                             shoppingQuantity = s.quantity,
                             bill=s.quantity*p.price
                         };
            foreach (var item2 in result)
            {
                Console.WriteLine(item2.productID + "\t " + item2.productName + "\t" + item2.shoppingQuantity+ "\t"+ item2.bill);
            }
            

          





        }
    }
}