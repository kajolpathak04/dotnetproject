﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<string> name=new List<string>();
            name.Add("kajol");
            name.Add("neha");
            name.Add("ruby");
            name.Add("ram");
            Console.WriteLine("********list of name*********");
            foreach( string item in name)
            {
                Console.WriteLine(item);
            }
            //remove a one element
            name.Remove("neha");
                Console.WriteLine("*****after removing a element*********");
            foreach (string item in name)
            {
                Console.WriteLine(item);
            }
            //contains to check a element is present or not
            bool check=name.Contains("shivam");
            Console.WriteLine($"*********shivam is present or not **********:{check}");
            //insert a element at particular index
            Console.WriteLine("****after insering a element at index 1**** ");
            name.Insert(1, "rajnish");
            foreach (string item in name)
            {
                Console.WriteLine(item);
            }
            //remove a element at particular index
            Console.WriteLine("*****Remove a element at 1 index*****");
            name.RemoveAt(1);
            foreach (string item in name)
            {
                Console.WriteLine(item);
            }
 



        } 
    }
}
