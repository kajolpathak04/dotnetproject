﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Productapp.Repository
{
    internal class ProductRepository
    {
        public int p_id;
        public string p_name;
        public int p_price;
        public ProductRepository(int p_id, string p_name, int p_price)
        {
            this.p_id = p_id;
            this.p_name = p_name;
            this.p_price = p_price;
        }   
    }
}
