﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
   public class Apple
    {
        public virtual void color()
        {
            Console.WriteLine("color of apple is red");
            
        }

    }
    public class Banana : Apple
    {
        public override void color()
        {
            Console.WriteLine("color of banana is yellow");
        }
    }
    public class Watermelon : Apple
    {
        public override void color()
        {
            Console.WriteLine("color of watermelon is green");
        }
    }

}