﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    internal class Book { 
    
        public void ViewDetail(string book_name,string author)
    {
        Console.WriteLine($"book detail::book name{book_name}\t author:{author}");
    }
        public void ViewDetail(string book_name, string author,int price)
        {
            Console.WriteLine($"book detail::book name{book_name}\t author:{author}\tprice:{price}");
        }
    }
}
