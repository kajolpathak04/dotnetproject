﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Book obj1 = new Book();
            Console.WriteLine("*******overloading***********");
            obj1.ViewDetail("morning miracel", "Anuj Kadam");//method overloading
            obj1.ViewDetail("Rich man", "John",900); //method overloading
            Console.WriteLine("*******overriding**************");

            Apple a = new Apple();
            a.color();//class A display method call
            Banana b = new Banana();
            b.color();//class B display method call
            Watermelon c = new Watermelon();
            c.color(); //class C display method call

            Console.WriteLine("*****overriding*******");
            Apple a1 = new Banana();
            a1.color();    //creating a child reference
            Apple b1 = new Watermelon();
            b1.color();

        }
    }
}
