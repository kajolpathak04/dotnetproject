﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace PasswordValidation
{
    internal class Validation
    {
        string pattern = @"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()]).{8,}$";
        public bool psd_validate(string psd)
        {
            if(psd != null)
            {
                return Regex.IsMatch(psd, pattern);
            }
            else
            {
                return false;
            }
        }
    }
}
