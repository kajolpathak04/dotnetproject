﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    internal interface Student
    {
        void stu_detail(int id ,string name);
        void work();

    }
    interface Employee
    {
        void emp_detail(int emp_id, string emp_name, string emp_dept);
        void work();
    }
}
