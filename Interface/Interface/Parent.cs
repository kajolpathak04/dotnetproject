﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    internal class Parent:Student,Employee
    {
        public void stu_detail(int id,string name)
        {
            Console.WriteLine($"student id=:{id}\tstudent name=:{name}");
        }
        //Ex[plicit Interface
        void Student.work()
        {
            Console.WriteLine("study");
        }
        public void emp_detail(int emp_id,string emp_name,string emp_dept)
        {
            Console.WriteLine($"Employee id=:{emp_id}\tEmployee name=:{emp_name}\t employee dept:{emp_dept}");
        }
        //Excplicit Interface
        void Employee.work()
        {
            Console.WriteLine("Working in office");
        }
    }

}
