﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Parent obj1 = new Parent();
            obj1.stu_detail(1,"kajol");
            Student stu = new Parent(); //reference type
            stu.work();
            Employee emp = new Parent(); //reference type
            obj1.emp_detail(101,"ram","dotnet");
            emp.work();
        }
    }
}
