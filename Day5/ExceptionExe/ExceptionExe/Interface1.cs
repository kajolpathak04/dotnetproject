﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionExe
{
    internal interface Interface1
    {
        void WriteContentsToFile(user user, string filename);
        List<string> ReadContentsFromFile(string filename);
        bool IsUsernameExists(string userName, string filename);
    }
}
