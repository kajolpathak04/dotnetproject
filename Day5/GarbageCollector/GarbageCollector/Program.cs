﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarbageCollector
{
    internal class Program
    {
        string name;
        public Program(string name)
        {
            this.name = name;
        }
        ~Program()
        {
            Console.WriteLine("destructor");
        }
        public void dispose()
        {
            Console.WriteLine("disposal");

            GC.SuppressFinalize(this);
        }

        static void Main(string[] args)
        {
            Program p1 = new Program("kajol");
            Program p2 = new Program("ruby");
            //to knowing a maximum number of 
            Console.WriteLine();
            Console.WriteLine($"generation{GC.GetGeneration(p1)}");
            p1.dispose();
        }
    }
}
