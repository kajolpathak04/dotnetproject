﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandling
{
    internal class UserdefinedException:Exception
    {
        public UserdefinedException(string msg):base(msg)
        {
            Console.WriteLine(msg);
        }
    }
}
