﻿using Task2.Repository;
using Task2.Model1;
namespace Task2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
          product_declared[] Product;
            display obj1 = new display();
            Product = obj1.getdetail();
            Console.WriteLine("All Data");
            foreach (product_declared item in Product)
            {
                Console.WriteLine($"product id:{item.id} \t product name:{item.name}\t category:{item.category}\t price:{item.price}");
            }
            Console.WriteLine("************only mobile category**************");
            foreach (product_declared item in Product)
            {
                if (item.category == "mobile")
                {
                    Console.WriteLine($"product id:{item.id} \t product name:{item.name}\t category:{item.category}\t price:{item.price}");
                }
            }

            Console.WriteLine("***********After Update**************");
            foreach (product_declared item1 in Product)
            {
                if (item1.category == "TV")
                {
                    item1.category = "LCD";

                }
            }
            foreach (product_declared item2 in Product)
            {
                Console.WriteLine($"product id:{item2.id} \t product name:{item2.name}\t category:{item2.category}\t price:{item2.price}");
            }
            Console.WriteLine("*********after deleting a single row********");
            for (int i = 0; i < Product.Length; i++)
            {
                if (Product[i].name == "")
                {
                    Product[i] = null;

                }


            }
            foreach (product_declared item2 in Product)
            {
                Console.WriteLine($"product id:{item2.id} \t product name:{item2.name}\t category:{item2.category}\t price:{item2.price}");
            }





        }


    }
}
