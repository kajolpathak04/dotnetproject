﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2.Model1
{
    internal class product_declared
    {
        public int id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public int price { get; set; }
        public product_declared(int id, string name, string category, int price)
        {
            this.id = id;
            this.name = name;
            this.category = category;
            this.price = price;
        }
    }
}
