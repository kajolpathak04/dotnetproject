﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structure
{
  public class Customer
    {
        public string c_name;
        public int c_id;
        public string c_email;
        public Customer(string c_name, int c_id, string c_email)
        {
            this.c_name = c_name;
            this.c_id = c_id;
            this.c_email = c_email;
        }   
    }
}
